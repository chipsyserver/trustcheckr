<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//use DB;
//use Hash;
Route::get('/admin-api-key-update', function (Request $request) {
  // $apiCat=104; $apiKey="BC9c7ZVWp6dTdMa4HCzrItFP4r7rDprLGdsWHWL6zuw";
   
  die(json_encode(DB::collection('users')->update(['delete'=>0])));
  // die(json_encode('dsfsd'));
    // DB::collection('apikeys') ->where('key',$apiKey)->increment('remcount', 10000);
    // DB::collection('apikeys') ->where('key',$apiKey)->increment('setcount', 10000);
    // die(json_encode("sadasd"));
     
  // die(json_encode(DB::collection('apikeys')->where('key',"0nXWWU3TXT3bXYENkKJd2g26y7VLtQWPlOzAk8ZtLO0")->update([ 'userId'=>"5b0fde279920b065a69211a7"]))); 
});



Route::post('/fbscore-generator', 'Api\FBScoreGenaratControl@genaratescore');
//Route::get('/facebook-score-generator', 'Api\FBScoreGenaratControl@genaratescore');


Route::group(['middleware' => ['web']], function () {
 
Route::post('/score-and-rate','Api\ScoreRating@score_and_rate');
Route::post('/test-score-and-rate','Api\ScoreRating@score_and_rate');


Route::group(['prefix' =>'whatsapp'], function() {
Route::post('/get-rating','Api\WhatsappControl@get_rateapi');
Route::post('/update-rating','Api\WhatsappControl@update_rateapi');
Route::post('/update-comments','Api\WhatsappControl@update_comments');




Route::post('/post-message','Api\WhatsappapiControl@post_message');
Route::post('/post-rating','Api\WhatsappapiControl@post_rating');
Route::post('/post-comment','Api\WhatsappapiControl@post_comment');
Route::get('/get-post','Api\WhatsappapiControl@get_post');



});



 
});

    Route::post('/getscore', 'Api\AdduserControl@genaratescore');
    Route::post('/fb-datainfo','Api\FbScraperControl@fbscraper');
    Route::post('/score-datainfo','Api\AdduserControl@scraper_score');

Route::group(['middleware' => ['AuthApi']], function () { 
    Route::post('/sample-score-api','Api\AdduserControl@scraper_score');
    Route::post('/mobile-api', 'Api\AdduserControl@genaratescore');
    Route::post('/datainfodata-api','Api\FbScraperControl@fbscraper');
    Route::post('/email-api','Api\SearchEmail@searchemail');
    Route::get('/phone-api', 'Api\StacksmobileControl@getmobiledetails');
    Route::get('/searchemail-api', 'Api\SearchEmailPicasaweb@searchemail');
    Route::get('/trustScore-new', 'Api\TrustScore@genaratescore');
    Route::get('/get-pan', 'Api\PanControl@get_pan');
Route::get('/get-dl', 'Api\DLControl@get_dl');
Route::get('/get-voter', 'Api\VoterControl@get_voter');

});

Route::post('/get-realfbid-byname','Api\FbScraperControl@realfbid');
Route::post('/get-realfbid-byid','Api\FbScraperControl@realfbid');
Route::post('/fb-realscore','Api\FbScraperControl@fbrealscore');


Route::post('/detectFaces', 'Api\UsersmatchControl@detectFaces');
Route::post('/image-compare', 'Api\UsersmatchControl@imagecompare');
Route::post('/string-compare', 'Api\UsersmatchControl@string_compare');
Route::post('/fb-returnurl', 'Api\FbScraperControl@fb_returnurl');

Route::get('/white-phone-api', 'Api\WhitePhoneControl@white_phone');




 

