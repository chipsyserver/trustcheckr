 @extends('Admin.layouts.master_layout')
@section('content')
<link rel="stylesheet" type="text/css" href="{{asset('Assets/parsley/parsley.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('Assets/jquery-confirm/jquery-confirm.css')}}">



<script type="text/javascript">
 
 if (!library)
   var library = {};

library.json = {
   replacer: function(match, pIndent, pKey, pVal, pEnd) {
      var key = '<span class=json-key>';
      var val = '<span class=json-value>';
      var str = '<span class=json-string>';
      var r = pIndent || '';
      if (pKey)
         r = r + key + pKey.replace(/[": ]/g, '') + '</span>: ';
      if (pVal)
         r = r + (pVal[0] == '"' ? str : val) + pVal + '</span>';
      return r + (pEnd || '');
      },
   prettyPrint: function(obj) {
      var jsonLine = /^( *)("[\w]+": )?("[^"]*"|[\w.+-]*)?([,[{])?$/mg;
      return JSON.stringify(obj, null, 3)
         .replace(/&/g, '&amp;').replace(/\\"/g, '&quot;')
         .replace(/</g, '&lt;').replace(/>/g, '&gt;')
         .replace(jsonLine, library.json.replacer);
      }
   };



   </script>
<section id="content" ng-controller="TrustScoreController" >
                <div class="container">
                     <div class="card" style="padding: 10px;">
                 
                              
                             
                 
                                     <div  class="row">
                                         
                                   <div class="col-sm-12">
                                       <h2> TrustScore Checker</h2>
                                            
                                  
        <form  name="trustscroe" >
            {{csrf_field()}}
            
                        <div class="col-md-6">
          
         				   <div class="box-body">



            

                               <div class="form-group">   
                      
                            <label for="username" ><br> Name</label>
                        
                                <div class="form-line ">
                                    <input  type="text" id="name" required="required" name="name" class="form-control" placeholder="name" value="" data-parsley-errors-container="#username_error">

                                </div>
                               <!--  <div id="username_error"></div> -->
                            </div>
                         
                              
                                 <div class="form-group">   
                      
                            <label for="username" ><br>email</label>
                        
                                <div class="form-line" >
                                    <input  type="email" id="email" required="required" name="email" class="form-control" placeholder="Email" value="" >

                                </div>
                               
                            </div>

                            <div class="form-group">   
                      
                            <label for="username" ><br>phone Number</label>
                        
                                <div class="form-line" >
                                    <input  type="text" id="phone_num" required="required" name="phone_num" class="form-control" placeholder="Phone Number" value="" >

                                </div>
                               
                            </div>


                             

                            <div class="resp-msg"></div>
                            <button type="button"  class="btn btn-primary m-t-15 waves-effect more_cnt">Submit</button>
                        
                   

  
  
  </div>
  </div>
  </form>
  </div>
  </div>
  </div>
  </div>
  </section>
 <script src="/admin/jscontrols/api_controls.js"></script>

<script src="{{asset('Assets/parsley/parsley.js')}}"></script>
<script src="{{asset('Assets/jquery-confirm/jquery-confirm.js')}}"></script>

@endsection