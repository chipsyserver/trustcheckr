 @extends('Admin.layouts.master_layout')
@section('content')
<link rel="stylesheet" type="text/css" href="{{asset('Assets/parsley/parsley.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('Assets/jquery-confirm/jquery-confirm.css')}}">
<section id="content" ng-controller="MatchController" >
                <div class="container">
                     <div class="card" style="padding: 10px;">
                 
                              
                             
                 
                                     <div  class="row">
                                         
                                   <div class="col-sm-12">
                                       <h2> Add Api Keys</h2>
                                            
                                  
        <form ng-enter="updateApi()" name="add_api" >
            {{csrf_field()}}
            
                        <div class="col-md-6">
          
         				   <div class="box-body">
            {{csrf_field()}}


            

                               <div class="form-group">   
                      
                            <label for="username" ><br>Api Name</label>
                        
                                <div class="form-line ">
                                    <input  type="text" id="name" required="required" name="name" class="form-control" placeholder="name" value="" data-parsley-errors-container="#username_error">

                                </div>
                               <!--  <div id="username_error"></div> -->
                            </div>
                             <div class="form-group">   
                              <label for="username" ><br>Api Type</label>
                              	
                                 <select class="form-control select2"  name="category" required="required" style="width: 100%;">
                  <option selected="selected">Select  Category</option>
                   @foreach(App\Apps_category::get() as $data)
                  <option value="{{$data->category_id}}">{{$data->category_name}}</option>
                  @endforeach 
                  </select>
                              
                              	 </div>

                                 <div class="form-group">   
                      
                            <label for="username" ><br>HIT COUNTS</label>
                        
                                <div class="form-line" >
                                    <input  type="text" id="count" required="required" name="count" class="form-control" placeholder="Count" value="" >

                                </div>
                               
                            </div>
                                    <div class="form-group">   
                              <label for="username" ><br>User</label>
                                
                                 <select class="form-control select2"  name="user"  style="width: 100%;">
                  <option selected="selected">Select User</option>
                   @foreach(App\User::where('roll_id',101)->where('delete',0)->get() as $data)
                  <option value="{{$data->_id}}">{{$data->username}}</option>
                  @endforeach 
                  </select>
                              
                                 </div>

                            <div class="resp-msg"></div>
                            <button type="button" ng-click="updateApi()" class="btn btn-primary m-t-15 waves-effect">Submit</button>
                        
                   

  
  
  </div>
  </div>
  </form>
  </div>
  </div>
  </div>
  </div>
  </section>
 <script src="/admin/jscontrols/api_controls.js"></script>

<script src="{{asset('Assets/parsley/parsley.js')}}"></script>
<script src="{{asset('Assets/jquery-confirm/jquery-confirm.js')}}"></script>

@endsection