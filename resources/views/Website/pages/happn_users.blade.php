@extends('Admin.layouts.master_layout')
@section('content')
            <section id="content">
                <div class="container" >
               
                    
                    <div class="card"  ng-controller="HappnController">
                        <div class="card-header">
                          <div class="col-md-8">  <h2>Welcome to Happn Controls </h2>  </div>

                                  <div class="col-md-4"><button class="btn btn-primary btn-sm pull-right" name="send-msg" style="margin-bottom: 5px;"> Send message  </button>
                                  </div>
                        </div>
                        <style type="text/css">
                          td{
                            word-break: break-all;
                          }

                        </style>
                               <table class="table table-bordered table-primary  mb30">
                                        <thead>
                                          <tr>
                                            <th width="5%">SLNO</th>
                                            <th width="15%">Name</th>
                                            <th width="30%">Token</th>
                                            <th width="10%">Latitude</th>
                                            <th width="10%">Longitude</th>
                                            <th width="5%"> State </th>
                                            <th width="25%">Controls</th>
                                          </tr>
                                        </thead>
                                        <tbody class="display_state">
                                          <?php $i=1; ?>
                                          @foreach($response as $info)
                                          <tr id="{{ (string)$info['_id'] }}">
                                           <td> {{  $i++ }}</td>
                                            <td>{{ $info['name'] }}</td>
                                            <td width="30%" >{{ $info['token'] }}</td>
                                            <td>{{ $info['lat'] }}</td>
                                            <td>{{ $info['log'] }} </td>
                                             <td> 
											 
											 @if($info['state']==0)   
                                             <div class="toggle-switch">
											 <input id="tw-switch{{ (string)$info['_id'] }}" type="checkbox" hidden="hidden" checked>
											 <label for="tw-switch{{ (string)$info['_id'] }}" class="ts-helper"></label>
											 </div>
											 @else  <div class="toggle-switch">
											 <input id="tw-switch{{ (string)$info['_id'] }}" type="checkbox" hidden="hidden" >
											 <label for="tw-switch{{ (string)$info['_id'] }}" class="ts-helper"></label>
											 </div> @endif
											 
											 </td>
                                           
                                            <td class="">
                                             
                                           <button type="button" name="view_profile_btn" onClick="window.open('/superadmin/happn/manage/{{  $info['user_id']  }}','_black')" class="btn btn-icon command-edit" data-row-id="10253"><span class="zmdi zmdi-eye zmdi-hc-fw"></span></button> 

                                            <button type="button" name="edit_profile_btn" class="btn btn-icon command-edit" data-row-id="10253"><span class="zmdi zmdi-edit"></span></button> 

                                            <button type="button" name="delete_state_btn" class="btn btn-icon command-delete" data-row-id="10253"><span class="zmdi zmdi-delete"></span></button></td>

                                           
                                          </tr>
                                         @endforeach
                                        </tbody>
                                         <tfoot>
                                         <tr >
                                          <td>#</td>
                                           <td ></td>
                                            <td ><textarea name="token" class="form-control input-sm"  type="text" required="required"  placeholder="Add Token"></textarea></td>
                                           

                                            <td><input name="lat" class="form-control input-sm" required="required"  placeholder="Add Latitude"/></td>
                                            <td><input name="log" class="form-control input-sm"  required="required"  placeholder="Add Longitude"/> </td>
                                             <td colspan="2"> <button class="btn btn-primary btn-sm" ng-click="insertProfile()">Add Profile </button></td>
                                          </tr>
                                         </tfoot>
                                    </table>
                    
                   
                    
                </div>
            </section>
        </section>
          
                <script src="/admin/jscontrols/happn_control.js"></script>
       @endsection
