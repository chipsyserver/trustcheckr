@extends('Admin.layouts.master_layout')
@section('content')

  
   <section id="content"  ng-controller="MatchController">
                <div class="container">
                   <div class="card">
                         <div role="tabpanel">
                          <div class="card-body card-padding">
                            <div class="card-body card-padding" style="padding-bottom: 0px;">
                             <ul class="tab-nav text-center fw-nav" role="tablist" tabindex="1" style="overflow: hidden; outline: none;">
                                    <li class="active"><a href="#home11" aria-controls="home11" role="tab" data-toggle="tab" aria-expanded="true">  User Score </a></li>
                                </ul>
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="home11"  name="search_matching">
                            
                            <div class="row  col-sm-offset-2">
                                      
                            <div class="col-md-8 col-sm-12">
                             <div class="col-md-4 col-sm-12"> 

                               <label style="    margin-top: 10px;">
                                   Image Type
                                  
                                </label>
                            </div>
                             <div class="col-md-4 col-sm-6">  
                               <div class="radio m-b-15">
                                <label>
                                    <input type="radio" name="image_type" value="1">
                                    <i class="input-helper"></i>Image 
                                  
                                </label>
                            </div> 
                            </div>
                                <div class="col-md-4 col-sm-6">  
                             <div class="radio m-b-15">
                                <label> 
                                    <input type="radio" name="image_type" value="2" checked="">
                                    <i class="input-helper"></i>
                                     Base64
                                </label>
                            </div>
                            </div>
                            </div>
 
                             <div class="col-md-4 col-sm-12">                     
                              &nbsp
                            </div>
                           </div>
                           <div class="row  col-sm-offset-2">
                                <div class="col-md-4 col-sm-12">                       
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="zmdi zmdi-image zmdi-hc-fw"></i></span>
                                        <div class="fg-line" name="image_input_cnt">
                                                <input type="text" class="form-control" name="image_base64" placeholder="Base64 image" required="true">
                                         </div>
                                       </div>
                                </div>
                                
                                
                              
                      
                                <div class="col-md-4 col-sm-12">
                                    <button class="btn btn-primary btn-icon-text waves-effect" name="get_score" ><i class="zmdi zmdi-search"></i> Get Score </button>
                                </div>
                                
                                     </div>
                                    </div>
                                   </div>
                                 </div>
                               </div>
                    <div style="display: none;    padding-bottom: 5px;" class="pre_json">
                     <h4 style="    padding-left: 20px;">  Response :</h4> 
                      <pre ><code id="json_code"> </code></pre>
                </div>
                </div>
                 </div>
                
                    
                </div>
             
               
             
             
             
            </section>
  
     <script src="/admin/jscontrols/api_controls.js"></script>
     @endsection
