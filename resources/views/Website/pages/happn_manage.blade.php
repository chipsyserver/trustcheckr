@extends('Admin.layouts.master_layout')
@section('content')
            <section id="content">
                <div class="container">
                
                    
                    <div class="card">
                        <div class="card-header">
                            <h2>User Dashboard </h2>

                              <br/ >
                               <ul>
                                  <li><a href="/superadmin/happn/manage/{{Request::segment(4)}}/getinfo?page=self" target="_blank">Self information</a></li>
                                  <li><a href="/superadmin/happn/manage/{{Request::segment(4)}}/getinfo?page=notif&op=first"  target="_blank">Notifications</a></li>
                                  <li><a href="/superadmin/happn/manage/{{Request::segment(4)}}/getinfo?page=accepted"  target="_blank">Accepted users</a></li>
                                  <li><a href="/superadmin/happn/manage/{{Request::segment(4)}}/getinfo?page=rejected"  target="_blank">Rejected users</a></li>
                                  <li><a href="/superadmin/happn/manage/{{Request::segment(4)}}/getinfo?page=conv&op=first"  target="_blank">Conversations</a></li>
                                  <li><a href="/superadmin/happn/manage/{{Request::segment(4)}}/getinfo?page=proof"  target="_blank">Proof (experimental)</a></li>
                                  <li><a href="/superadmin/happn/manage/{{Request::segment(4)}}/getinfo?page=achievement-types"  target="_blank">Achievements types (experimental)</a></li>
                                  <li><a href="/superadmin/happn/manage/{{Request::segment(4)}}/getinfo?page=achievements"  target="_blank">Achievements (experimental)</a></li>
                                  <li><a href="/superadmin/happn/manage/{{Request::segment(4)}}/getinfo?page=report-types"  target="_blank">Report types (experimental)</a></li>
                                  <li><a href="/superadmin/happn/manage/{{Request::segment(4)}}/getinfo?page=reports"  target="_blank">Reports (experimental)</a></li>
                                  <li><a href="/superadmin/happn/manage/{{Request::segment(4)}}/getinfo?page=devices"  target="_blank">Devices (experimental)</a></li>
                               
                                </ul>
                        </div>
                        
                     
                    
                   
                    
                </div>
            </section>
        </section>
             @endsection
