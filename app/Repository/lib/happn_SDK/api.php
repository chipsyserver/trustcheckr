<?php

require_once ('serv.php');
require_once ('json.php');


// Happn REST API

class Happn extends Service
  {
  public function __construct ()
    { 
  
    parent::__construct ('https://api.happn.fr');
    }
  
  
  private $client_id     = 'FUE-idSEP-f7AqCyuMcPr2K-1iCIU_YlvK-M-im3c';
  private $client_secret = 'brGoHSwZsPjJ-lBk0HqEXVtb3UFu-y5l_JcOjD-Ekv';

  public $device_id      = NULL;


  private function fb_load ()
    {
    while (TRUE)
      {
      $m = json_load ('../lib/fb.json');
      if (!$m) break;

      if (isset ($m ['fb_token'])) $GLOBALS['FB_TOKEN'] = $m ['fb_token'];

      break;
      }
    }


  // Load authentication data from JSON file

  private function auth_load ()
    {
    while (TRUE)
      {
      $m = json_load ('../lib/auth.json');
      if (!$m) break;

      if (isset ($m ['access_token']))  $GLOBALS['ACCESS_TOKEN']  = $m ['access_token'];
      if (isset ($m ['refresh_token'])) $GLOBALS['REFRESH_TOKEN'] = $m ['refresh_token'];
      if (isset ($m ['expires_at']))    $GLOBALS['EXPIRE_TIME']   = $m ['expires_at'];
      if (isset ($m ['user_id']))       $GLOBALS['USER_ID']       = $m ['user_id'];

      break;
      }
    }


  // Save authentication data to JSON file

  private function auth_save ()
    {
    $r = FALSE;

    while (TRUE)
      {
      $m = array ();

      if (!is_null ($GLOBALS['ACCESS_TOKEN']))  $m ['access_token']  = $GLOBALS['ACCESS_TOKEN'];
      if (!is_null ($GLOBALS['REFRESH_TOKEN'])) $m ['refresh_token'] = $GLOBALS['REFRESH_TOKEN'];
      if (!is_null ($GLOBALS['EXPIRE_TIME']))    $m ['expires_at']    = $GLOBALS['EXPIRE_TIME'];
      if (!is_null ($GLOBALS['USER_ID']))       $m ['user_id']       = $GLOBALS['USER_ID'];

      $r = json_save ('../lib/auth.json', $m);
      break;
      }

    return $r;
    }


  // Load device data from JSON file

  private function dev_load ()
    {
    while (TRUE)
      {
      $m = json_load ('../lib/dev.json');
      if (!$m) break;

      if (isset ($m ['device_id'])) $this->device_id = $m ['device_id'];

      break;
      }
    }


  // Save device data to JSON file

  private function dev_save ()
    {
    $r = FALSE;

    while (TRUE)
      {
      $m = array ();

      if (!is_null ($this->device_id)) $m ['device_id'] = $this->device_id;

      $r = json_save ('../lib/dev.json', $m);
      break;
      }

    return $r;
    }


  // Create access & refresh token from FB token

  private function token_create ()
    {
    $h = array (
      'Content-Type: application/x-www-form-urlencoded; charset=utf-8'
      );

    $m = array (
      'client_id'      => $this->client_id,
      'client_secret'  => $this->client_secret,
      'grant_type'     => 'assertion',
      'assertion_type' => 'facebook_access_token',
      'assertion'      => $GLOBALS['FB_TOKEN'],
      'scope'          => 'mobile_app'
      );

    $b = http_build_query ($m);

    $r = $this->exec ('POST', '/connect/oauth/token', $h, $b);
    $m = json_decode ($r, TRUE);

    if (isset ($m ['error_code']) && $m ['error_code'] == 0)
      {
      $GLOBALS['ACCESS_TOKEN']  = $m ['access_token'];
      $GLOBALS['REFRESH_TOKEN'] = $m ['refresh_token'];
      $GLOBALS['EXPIRE_TIME']   = time () + $m ['expires_in'];
      $GLOBALS['USER_ID']       = $m ['user_id'];
      }

    return $m;
    }


  // Refresh access & refresh token

  private function token_refresh ()
    {
    $h = array (
      'Content-Type: application/x-www-form-urlencoded; charset=utf-8'
      );

    $m = array (
      'grant_type'    => 'refresh_token',
      'refresh_token' => $GLOBALS['REFRESH_TOKEN'],
      'client_id'     => $this->client_id,
      'client_secret' => $this->client_secret
      );

    $b = http_build_query ($m);

    $r = $this->exec ('POST', '/connect/oauth/token', $h, $b);
    $m = json_decode ($r, TRUE);

    if (isset ($m ['error_code']) && $m ['error_code'] == 0)
      {
      $GLOBALS['ACCESS_TOKEN']  = $m ['access_token'];
      $GLOBALS['REFRESH_TOKEN'] = $m ['refresh_token'];
      $GLOBALS['EXPIRE_TIME']   = time () + $m ['expires_in'];
      $GLOBALS['USER_ID']       = $m ['user_id'];
      }

    return $m;
    }


  // Authentication procedure

  public function auth ($delay = 0)
    {
    $res = FALSE;

    while (TRUE)
      {
      $this->fb_load ();
      $this->auth_load ();

      // First time authentication

      if (is_null ($GLOBALS['USER_ID']))
        {
        if (is_null ($GLOBALS['FB_TOKEN'])) break;

        $m = $this->token_create ();
        if (isset ($m ['error_code']) && $m ['error_code'] == 0)
          {
          $this->auth_save ();
          $res = TRUE;
          }

        break;
        }

      // Check expiration time and refresh tokens
      // with delay needed for script completion

      if ($GLOBALS['EXPIRE_TIME']<= time () + $delay)
        {
        $GLOBALS['ACCESS_TOKEN'] = NULL;

        $m = $this->token_refresh ();
        if (isset ($m ['error_code']) && $m ['error_code'] == 0)
          {
          $this->auth_save ();
          $res = TRUE;
          }

        break;
        }

      // Tokens are still valid

      $res = TRUE;
      break;
      }

    return $res;
    }


  // API invocation

  private function invoke ($method, $url, $headers = NULL, $body = NULL)
    {
    // Common headers
     

    $h = array (
      'User-Agent: Happn/19.1.0 AndroidSDK/19',
      'Authorization: OAuth="' . $GLOBALS['ACCESS_TOKEN'] . '"'
      );

    // Issue #11 : new header with device identifier

    if (!is_null ($this->device_id))
      $h [] = 'X-Happn-DID: ' . $this->device_id;

    if (!is_null ($headers))
      {
      $h = array_merge ($h, $headers);
      }

    $r = $this->exec ($method, $url, $h, $body);
    return json_decode ($r, TRUE);
    }


  // Confirm Facebook token
  // Return an 'app_secret_proof' key
  // What is the purpose of such key ?

  public function proof ()
    {
    $h = array (
      'Content-Type: application/x-www-form-urlencoded; charset=utf-8'
      );

    $m = array (
      'facebook_access_token' => $GLOBALS['FB_TOKEN']
      );

    $b = http_build_query ($m);

    $r = $this->invoke ('POST', '/api/auth/proof', $h, $b);
    return $r;
    }


  // Achievements

  public function achievement_types ()
    {
    $r = $this->invoke ('GET', '/api/achievement-types/');
    return $r;
    }

  public function achievements ()
    {
    $r = $this->invoke ('GET', '/api/users/' . $GLOBALS['USER_ID'] . '/achievements/');
    return $r;
    }


  // Reports

  public function report_types ()
    {
    $r = $this->invoke ('GET', '/api/report-types/');
    return $r;
    }

  public function reports ()
    {
    $r = $this->invoke ('GET', '/api/users/' . $GLOBALS['USER_ID'] . '/reportss/');
    return $r;
    }


  // Set position (old way)

  public function pos ($latitude, $longitude)
    {
    $h = array (
      'Content-Type: application/json'
      );

    // Looks like Happn hates high precision
    // Use same precision as Google Maps

    $latitude  = round ($latitude,  6);
    $longitude = round ($longitude, 6);

    $m = array (
      'alt'       => 0.0,
      'latitude'  => $latitude,
      'longitude' => $longitude
      );

    $b = json_encode ($m);

    $r = $this->invoke ('POST', '/api/users/' . $GLOBALS['USER_ID'] . '/position/', $h, $b);
    return $r;
    }


  // Get list of devices

  public function devices ()
    {
    $r = $this->invoke ('GET', '/api/users/' . $GLOBALS['USER_ID'] . '/devices/');

    if (isset ($r ['success']) && $r ['success'])
      {
      // Store the first device ID for positionning

      $devs = $r ['data'];
      if (is_array ($devs) && count ($devs) > 0)
        {
        $dev = $devs [0];
        $this->device_id = $dev ['id'];
        }
      }

    return $r;
    }



// Get list of devices

  public function send_message ($conId,$msg)
    {
	  
	  $h = array (
      'Content-Type: application/json'
      );

      $m = array (
        'fields' => "message,creation_date,sender.fields(id)",
        'message' => $msg,
       );

      $b = json_encode ($m);
      $r = $this->invoke ('POST','/api/conversations/'.$conId.'/messages', $h, $b);
      return $r;
   }
    
   

  /*
  // TODO: tune this function after understanding how device is created

  public function dev ()
    {
    $headers = array (
      'Content-Type'  => 'application/json'
      );

    $map = array (
      'app_build'   => '19.1.0',
      'country_id'  => 'FR',
      'gps_adid'    => '05596566-c7c7-4bc7-a6c9-729715c9ad98',
      'idfa'        => 'f550c51fa242216c',
      'language_id' => "fr",
      'os_version'  => '19',
      'token'       => 'APA91bE3axREMeqEpvjkIOWyCBWRO1c4Zm69nyH5f5a7o9iRitRq96ergzyrRfYK5hsDa_-8J35ar7zi5AZFxVeA6xfpK77_kCVRqFmbayGuYy7Uppy_krXIaTAe8Vdd7oUoXJBA7q2vVnZ6hj9afmju9C3vMKz-KA,',
      'type'        => 'android'
      );

    $body = json_encode ($map);

    $r = $this->invoke ('PUT', '/api/users/' . $GLOBALS['USER_ID'] . '/devices/' . ??? , $headers, $body);
    return json_decode ($r, TRUE);
    }
    */


  // Get user information

  public function user_get ($fields, $id = NULL)
    {
    $m = array (
      'fields' => $fields
      );

    $q = http_build_query ($m);

    if (is_null ($id)) $id = $GLOBALS['USER_ID'];

    $r = $this->invoke ('GET', '/api/users/' . $id . '?' . $q);
    return $r;
    }


  // Accepting

  public function accept ($user_id)
    {
    $h = array (
      'Content-Type: application/json'
      );

    $m = array ( );

    $b = json_encode ($m);

    $r = $this->invoke ('POST', '/api/users/' . $GLOBALS['USER_ID'] . '/accepted/'.$user_id, $h, $b);
   
    return $r;
    }


  public function unaccept ($user_id)
    {
    $r = $this->invoke ('DELETE', '/api/users/' . $GLOBALS['USER_ID'] . '/accepted/' . $user_id);
    return $r;
    }


  public function accepted ($fields, $offset = 0, $limit = 10)
    {
    $m = array (
      'offset' => $offset,
      'limit' => $limit,
      'fields' => $fields
      );

    $q = http_build_query ($m);

    $r = $this->invoke ('GET', '/api/users/' . $GLOBALS['USER_ID'] . '/accepted/?' . $q);
    return $r;
    }


  // Rejecting

  public function reject ($user_id)
    {
    $h = array (
      'Content-Type: application/json'
      );

    $m = array (
      'id' => $user_id
      );

    $b = json_encode ($m);

    $r = $this->invoke ('POST', '/api/users/' . $GLOBALS['USER_ID'] . '/rejected', $h, $b);
    return $r;
    }


  public function unreject ($user_id)
    {
    $r = $this->invoke ('DELETE', '/api/users/' . $GLOBALS['USER_ID'] . '/rejected/' . $user_id);
    return $r;
    }


  public function rejected ($fields, $offset = 0, $limit = 10)
    {
    $m = array (
      'offset' => $offset,
      'limit' => $limit,
      'fields' => $fields
      );

    $q = http_build_query ($m);

    $r = $this->invoke ('GET', '/api/users/' . $GLOBALS['USER_ID'] . '/rejected/?' . $q);
    return $r;
    }


  // Notifications

  public function notif ($fields, $offset = 0, $limit = 10)
    {
      $m = array (
      'types' => 468,
      'offset' => $offset,
      'limit' => $limit,
      'fields' => $fields
      );
   // die(json_encode($limit));
    $q = http_build_query ($m);

    $r = $this->invoke ('GET', '/api/users/' . $GLOBALS['USER_ID'] . '/notifications/?' . $q);
    return $r;
    }


    public function conv2 ($fields, $offset = 0, $limit = 10)
    {
    $m = array (
      'offset' => $offset,
      'limit' => $limit,
      'fields' => $fields
      );

    $q = http_build_query ($m);

    $r = $this->invoke ('GET', '/api/users/' . $GLOBALS['USER_ID'] . '/conversations/?' . $q);
    return $r;
    }


  // Conversations

  public function conv ()
    {
      $m = array (
      'offset' => 0,
      'limit' => 1000
      );
      $q = http_build_query ($m);
      $r = $this->invoke ('GET', '/api/users/' . $GLOBALS['USER_ID'] . '/conversations/?' . $q);
      return $r;
    }
    
    
    

  // Initialize

  public function init ($delay)
    {
    $r = FALSE;

    while (TRUE)
      {
      // Authentication

      $r = $this->auth ($delay);
      if (!$r) break;

      // Get device data

      $this->dev_load ();
      if (is_null ($this->device_id))
        {
        $m = $this->devices ();
        if (isset ($m ['success']) && $m ['success'])
          {
          $this->dev_save ();
          }
        }

      $r = TRUE;
      break;
      }

    return $r;
    }

  }  // Happn REST API

?>
