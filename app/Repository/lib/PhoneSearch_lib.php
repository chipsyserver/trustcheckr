<?php
namespace App\Repository\Lib;
use App\Http\Controllers\Controller;
use App\MasterPhone;
use DB; 
class PhoneSearch_lib extends Controller
{
    
    public static function phonesearchlib_func($data){ 

         $response['status']['code']=0;
         $response['status']['message']="Users details found.";
         $response['status']['data']=array();
           //$data['phone_num']="918867265210";
         try {          
			  
          $result=file_get_contents('https://api.datayuge.com/peoplestacks?apikey=i7iHXFgQm3BM5Yg6L365kvSuBC012fUIJrm&phone='.$data['phone_num']);
          $result =json_decode($result, true);
             


           //die(json_encode( $result['data']['value']['name']));
          // die(json_encode($result['data']['value']['number_details'][0]['country_calling_code']));

           $master_mobile =  MasterPhone::firstOrNew(['mobile' => $result['data']['key']]); 
           $master_mobile->mobile  =isset($result['data']['key'])?$result['data']['key']:"";
           $master_mobile->tcname  =isset($result['data']['value']['name'])?$result['data']['value']['name']:"";
           
           $master_mobile->area    =isset($result['data']['value']['location_details'][0]['city'])?$result['data']['value']['location_details'][0]['city']:"";
           $master_mobile->city    =isset($result['data']['value']['location_details'][0]['city'])?$result['data']['value']['location_details'][0]['city']:"";
           $master_mobile->countryCode =isset($result['data']['value']['number_details'][0]['country_calling_code'])?$result['data']['value']['number_details'][0]['country_calling_code']:"";

           $master_mobile->email    =isset($result['data']['value']['social_details'][0]['email'])?$result['data']['value']['social_details'][0]['email']:"";

           $master_mobile->operator =isset($result['data']['value']['number_details'][0]['default_carrier'])?$result['data']['value']['number_details'][0]['default_carrier']:"";
           $master_mobile->facebook_id = isset($result['data']['value']['social_details'][1]['facebook_id'])?$result['data']['value']['social_details'][1]['facebook_id']:"";
           $master_mobile->timage   = isset($result['data']['value']['social_details'][1]['facebook_image'])?$result['data']['value']['social_details'][1]['facebook_image']:"";
           $master_mobile->response =$result;
           $master_mobile->save();
         
         
       
           
        $result['data']['value']['social_details'][1]['facebook_image']= isset($result['data']['value']['social_details'][1]['facebook_id'])?'https://graph.facebook.com/'.$result['data']['value']['social_details'][1]['facebook_id'].'/picture?type=large':"";
             
       if(isset($GLOBALS['apikeyID'])){

                $ipaddress = '';
                if (isset($_SERVER['HTTP_CLIENT_IP']))
                    $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
                else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
                    $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
                else if(isset($_SERVER['HTTP_X_FORWARDED']))
                    $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
                else if(isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
                    $ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
                else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
                    $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
                else if(isset($_SERVER['HTTP_FORWARDED']))
                    $ipaddress = $_SERVER['HTTP_FORWARDED'];
                else if(isset($_SERVER['REMOTE_ADDR']))
                    $ipaddress = $_SERVER['REMOTE_ADDR'];
                else
                    $ipaddress = 'UNKNOWN';
          

           $apikeyID=$GLOBALS['apikeyID'];
           $insert_db=array(
              'apikeyID'=>$apikeyID,
              'requesttime'=>time(),
              'requestIP'=>$ipaddress,
              'response' =>$result['data'],

               );
       
        
           DB::collection('apikeys')->where('_id',$apikeyID)->decrement('remcount');
           DB::collection('apis_analytics')->insertGetId($insert_db);

           }
         $response['status']['data']=  $result['data'];
       } catch (\Exception $e) {
                 $response['status']['code']=2;
                 $response['status']['message']="No data Found.";
                  $response['status']['data']=array();

           }
           return  $response;
        
               
    }


   
    
}
