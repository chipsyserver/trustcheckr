<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Auth;

class superadmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
   

    public function handle($request, Closure $next)
    {   
     
    
      if (Auth::guard("superadmin")->check()) {

                if (Auth::guard("superadmin")->user()->roll_id==100) {

             return $next($request);

               
            } }
            else{

                 return redirect('/superadmin');

                  }
        
        
    }
}
