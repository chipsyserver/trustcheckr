<?php

namespace App\Http\Middleware;
use Closure;
use Illuminate\Support\Facades\Auth;
use DB;

class  AuthenticatedApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null )
    {  
     // $headers = apache_request_headers();
     // die(json_encode(DB::collection('apikeys')->where('apiId',105)->where('key','Hkxlttkir9WfgujnRiQt3L+REWkyfdWBxz80AE/gXfQ=')->update(['key'=>'Ie5JtImyHR9ORznuJLCocVP5ryURpz0Qv8gt44fTf1Q'])));
      if (isset($_GET['apiKey'])) {
         $apiKey= $_GET['apiKey'];
         $GLOBALS['apikey']=$apiKey;

         $apiCat= $request->url();
         $urls=explode("/",$apiCat);
         $url= isset($urls[4]) ?$urls[4]:"";
           
         switch ($url) {
            case 'mobile-api':
                 $apiCat=100;
                break;
            case 'fbscraper-api':
                $apiCat=101;
                break;
            case 'sample-score-api':
                $apiCat=102;
                break;
            case 'email-api':
                $apiCat=103;
                break;  
                
           case 'phone-api':
                 $apiCat=104;
                break;  
           case 'searchemail-api':
                 $apiCat=105;
                break;  
           case 'trustScore-new':
                 $apiCat=106;
                break;  
            case 'get-pan':
                 $apiCat=107;
                break; 
                  case 'get-dl':
                 $apiCat=108;
                break;    
                case 'get-voter':
                 $apiCat=109;
                break; 
                        
            default:
                 $response['status']['code'] = 1024;
                 $response['status']['message'] = "Invalid request.";
                 die(json_encode($response ));
                break;
            }
 //die(json_encode( $apiCat));
           $getkeys= DB::collection('apikeys')->where('apiId', $apiCat)->where('key',$apiKey)->get();
    //die(json_encode(   $getkeys));
         if(0== count( $getkeys)) {
              $response['status']['code'] = 1024;
              $response['status']['message'] = "Invalid request.";
              die(json_encode($response ));
         }else if($getkeys[0]['remcount']<=0){
       //  die(json_encode(DB::collection('apikeys')->where('apiId', $apiCat)->where('key',$apiKey)->update(['remcount'=>5000,'setcount'=>5000])));
              $response['status']['code'] = 1025;
              $response['status']['message'] = "Limit exceeded. contact customer care for more hits.";
              die(json_encode($response ));
          }
     //die(json_encode(DB::collection('apikeys')->where('apiId', $apiCat)->where('key',$apiKey)->update(['remcount'=>500,'setcount'=>500])));
    
          $GLOBALS['apikeyID']= (string)$getkeys[0]['_id'];
       
          return $next($request);

       }else{
           $response['status']['code'] = 1024;
           $response['status']['message'] = "Invalid request.";
           die(json_encode($response ));
       }
    }
}
