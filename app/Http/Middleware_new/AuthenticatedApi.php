<?php

namespace App\Http\Middleware;
use Closure;
use Illuminate\Support\Facades\Auth;
use DB;

class  AuthenticatedApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null )
    {
      $headers = apache_request_headers();
      if (isset($headers['apiKey'])) {
         $apiKey= $headers['apiKey'];
         $GLOBALS['apikey']=$apiKey;

         $apiCat= $request->url();
         $urls=explode("/",$apiCat);
         $url= isset($urls[4]) ?$urls[4]:"";
         switch ($url) {
            case 'mobile-api':
                 $apiCat=100;
                break;
            case 'fbscraper-api':
                $apiCat=101;
                break;
            case 'sample-score-api':
                $apiCat=102;
                break;
            case 'email-api':
                $apiCat=103;
                break;     
            default:
                 $response['status']['code'] = 1024;
                 $response['status']['message'] = "Invalid request.";
                 die(json_encode($response ));
                break;
            }
 //die(json_encode( $apiCat));
          //  die(json_encode(DB::collection('apikeys')->where('apiCat',$apiCat)->count()));
           $getkeys= DB::collection('apikeys')->where('apiId', $apiCat)->where('key',$apiKey)->get();
    
         if(0== count( $getkeys)) {
              $response['status']['code'] = 1024;
              $response['status']['message'] = "Invalid request.";
              die(json_encode($response ));
         }

          $GLOBALS['apikeyID']= (string)$getkeys[0]['_id'];
          return $next($request);

       }else{
           $response['status']['code'] = 1024;
           $response['status']['message'] = "Invalid request.";
           die(json_encode($response ));
       }
    }
}
