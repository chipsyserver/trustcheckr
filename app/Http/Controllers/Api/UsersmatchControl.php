<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Storage;
use Aws\Rekognition\RekognitionClient;
 
class UsersmatchControl extends Controller {
   
   
   	public function detectFaces(Request $request){     
		$inputs    = $request->all();
        $rules     = array(
            'image_type' => 'required',
            'image_info' => 'required',
		 
        );
        $validator = Validator::make($request->all(), $rules);
        $data      = $request->all();
        if ($validator->fails()) {
         $response['status']['code']=1;
         $response['status']['message']="required missing .";
            return response()->json($response);
        }
        
         if($data['image_type']==1){
                   $data['image_base64']=base64_encode(file_get_contents($data['image_info']));
         }else $data['image_base64']=$data['image_info'];
        
        
        $res['requests'] = array();
        $res['requests']['image']['content'] = $data['image_base64'];
        $res['requests']['features'] = array();
        $res['requests']['features'][0]['type'] = "TEXT_DETECTION";
        $res['requests']['features'][1]['type'] = "SAFE_SEARCH_DETECTION";
        $res['requests']['features'][2]['type'] = "WEB_DETECTION";
        $data_string = json_encode($res);
         $visionAPiAuth= env('VisionAPiAuth');      
        $curl = curl_init('https://vision.googleapis.com/v1/images:annotate?key='.$visionAPiAuth);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json',));
        $result = curl_exec($curl);
        $result = json_decode($result, true);
        
         if (isset($result['error'])) {
            $info['status']['code'] = 2;
            $info['status']['message'] = "Invalid base64 image.";
            die(json_encode($info));
        }
        if (isset($result['responses'][0]['error'])) {
            $response['status']['code'] = 3;
            $response['status']['message'] = "Bad image data.";
            return response()->json($response);
        }
        $vresult = $result;
        
         
        $image = base64_decode($data['image_base64']);
        //  die(json_encode("sad"));    
        $img1 = time().'.jpg';
        $t = Storage::disk('s3')->put($img1,$image, 'public');
        $source_image = Storage::disk('s3')->url($img1);
        // die(json_encode($source_image));
             try {
       
            $client = RekognitionClient::factory(array(    
                         'credentials' => array(       
                         'key'    => env('S3_KEY'),
                         'secret' =>  env('S3_SECRET')),  
                         'region' =>  env('S3_REGION'),  
                         'version'=> 'latest',   
                      ));
          
        $result = $client->detectFaces([
            'Attributes' => ['ALL'],
            'Image' => [ // REQUIRED
            'Bytes' => file_get_contents($source_image),
        ],
       
       ]);  
        } catch (\Exception $e) {
                              $matching="";
                   }
           $fakePhoto=false;
            if (isset($vresult['responses'][0]['webDetection']['pagesWithMatchingImages'])) {
          
            if (strpos($vresult['responses'][0]['webDetection']['pagesWithMatchingImages'][0]['url'], 'linkedin') !== true) {
                $fakePhoto=true;
            }  
          }
         $safeVision=$vresult['responses'][0]['safeSearchAnnotation'];
         $response['status']['code']=0;
         $response['status']['message']="Face detectFaces result";
         $response['data']['fakePhoto']=$fakePhoto;
         $response['data']['safeVision']=$safeVision;
         $response['data']['AgeRange']=isset($result['FaceDetails'][0]['AgeRange'])?$result['FaceDetails'][0]['AgeRange']:array();
         $response['data']['Gender']=isset($result['FaceDetails'][0]['Gender'])?$result['FaceDetails'][0]['Gender']:array();
         die(json_encode($response));
      // die(json_encode($result));     
       
	}
    public function imagecompare(Request $request) {
        $inputs    = $request->all();
        $rules     = array(
            'image_type' => 'required',
            'image_info1' => 'required',
            'image_info2' => 'required',
		 
        );
        $validator = Validator::make($request->all(), $rules);
        $data      = $request->all();
        if ($validator->fails()) {
         $response['status']['code']=1;
         $response['status']['message']="required missing .";
            return response()->json($response);
        }
        
         if($data['image_type']==1){
                   $data['image_base64_1']=base64_encode(file_get_contents($data['image_info1']));
                   $data['image_base64_2']=base64_encode(file_get_contents($data['image_info2']));
         }else {
			 $data['image_base64_1']=$data['image_info1'];
			 $data['image_base64_2']=$data['image_info2'];
		 }
         
        $image = base64_decode($data['image_base64_1']);
        $img1 = time().'.jpg';
        $t = Storage::disk('s3')->put($img1,$image, 'public');      
        $source_image = Storage::disk('s3')->url($img1);
        
        
        $image = base64_decode($data['image_base64_2']);
        $img2 = time().'.jpg';
        $t = Storage::disk('s3')->put($img2,$image, 'public');
        $source_image = Storage::disk('s3')->url($img2);
    
         try {
                $client = RekognitionClient::factory(array(    
                         'credentials' => array(       
                         'key'    => env('S3_KEY'),
                         'secret' =>  env('S3_SECRET')),  
                         'region' =>  env('S3_REGION'),  
                         'version'=> 'latest',   
                      ));
                           $matching= $client->compareFaces([
                        'SimilarityThreshold' => 10,
                        'SourceImage' => [
                            'S3Object' => [
                                'Bucket' => 'usermatchin1',
                                'Name' => 'usermatchin1.s3-accelerate/'.$img1,
                            ],
                        ],
                        'TargetImage' => [
                            'S3Object' => [
                                'Bucket' => 'usermatchin1',
                                'Name' => 'usermatchin1.s3-accelerate/'.$img2,
                            ],
                        ],
                    ]);
                     } catch (\Exception $e) {
                              $matching="";
                   }
                         
                         
			 $user_dt['Similarity']=isset($matching['FaceMatches'][0]['Similarity'])?ceil($matching['FaceMatches'][0]['Similarity']) :"Not match";	 
      
         $response['status']['code']=0;
         $response['status']['message']="Face compareFaces result";
         $response['data']['similarity']= $user_dt['Similarity'];
         die(json_encode(  $response));

 
    }
    
     public function string_compare(Request $request) {
        $inputs    = $request->all();
        $rules     = array(
            'string1' => 'required',
            'string2' => 'required',
		);
        $validator = Validator::make($request->all(), $rules);
        $data      = $request->all();
        if ($validator->fails()) {
         $response['status']['code']=1;
         $response['status']['message']="required missing .";
            return response()->json($response);
        }
        
         similar_text($data['string1'], $data['string2'] ,$percent);
         $response['status']['code']=0;
         $response['status']['message']="string compare result";
         $response['data']['similarity']= $percent;
         die(json_encode(  $response));
	}
}
