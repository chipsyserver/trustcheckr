<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use DB;
use App\MasterPhone;
use App\Repository\lib\PhoneSearch_lib;
class StacksmobileControl extends Controller
{ 
    
      public function getmobiledetails(Request $request) {
       
        $rules = array(
             'phone_num' => 'required',
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
          $res = $validator->getMessageBag()->toArray();
            foreach ($res as $key => $value) {
                $response['status']['message'] = $value[0];
            }
            $response['status']['message'] = $value[0];
            return response()->json($response);
        } 
        $data = $request->all();
        $response=PhoneSearch_lib::phonesearchlib_func($data);
        die(json_encode( $response));
      }
    
    
   
   
}
