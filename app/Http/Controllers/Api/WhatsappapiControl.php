<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use DB;
use Auth;
use App\Repository\Lib\CurlControl;
use Session;
use Storage;
use App\Repository\Lib\TimeControl;
require_once app_path().'/Repository/lib/image.compare.class.php';
class WhatsappapiControl extends Controller {
    
  
    public function post_message(Request $request) {
        $inputs = $request->all();
        $rules = array('msg_type' => 'required');
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response['status']['code'] = 1;
            $res=$validator->getMessageBag()->toArray();
            foreach ($res as $key => $value) {  $response['status']['message']=$value[0];}
            return response()->json($response);
        }

        $data = $request->all();
        $tags_array=array();
        $userId ="";  $comments = array();
        if (isset($data['userId'])) {
            $userId = trim($data['userId']);
        }
        
    
        if (isset($data['msg_info'])) {
           $data['msg_info'] = trim($data['msg_info']);
        }else    $data['msg_info']="";
        
             $downcount=$upcount=$cmt_count=0;  
             $img_found=false;
             $image_url =  "";
        if ($data['msg_type'] == 1 ) {
            
            if(!isset($data['image_info'])){
                  $response['status']['code'] = 1;
                  $response['status']['message']='image is not found';
                  
            }
            $data['image_base64'] = base64_encode(file_get_contents($data['image_info']));
            $img1 = "truechecker/".time().'.jpg';
            $image = base64_decode($data['image_base64']);
            $t = Storage::disk('s3')->put($img1,$image, 'public');
            $source_image = Storage::disk('s3')->url($img1);
             
            $images=DB::collection('image_collection')->get(); 
            $imgurl=$source_image;
          
            $class   = new \compareImages(); 
            foreach ($images as  $imgdata) {

                $compare = $class->compare($source_image,$imgdata['imgurl']);  
                if( $compare==0 ||  $compare ==4) {
                   $img_found=true;
                   $imgid=(string)$imgdata['imgid'];
                   $imgurl= $imgdata['imgurl'];
                   break;

                }
             }

         
              $line1 = $data['msg_info'];
              $line2 =  "";
              $line3 =  ""; 
              $image_url =  $imgurl;
              $ocr_actual = $line1;

        } else { $line1 = $data['msg_info'];
              
              $line2 =  "";
              $line3 =  ""; $ocr_actual=$line1;
             
               
          }
              
                if ($img_found) {
             
                $post_data = DB::collection('post_collection')->where('_id',$imgid)->get();
              //  die(json_encode(  $imgid ));
                $total = 0;
                $rate = 0; // die(json_encode(  $res));
                foreach ($post_data[0]['rating'] as $value) {
                    $total++;
                    $rate = $rate + $value['rate'];
                switch ($value['rate']) {
                  case 1 :
                   $downcount++;
                    break;
                 case 0 :
                     $upcount++;
                    break;
                
                  }
                }

                if($rate!=0) $rating = $rate / $total;  else  $rating =0;

                $id=(string)$post_data[0]['_id'];
                $line1 = $post_data[0]['line1'];
                $line2 = $post_data[0]['line2'];
                $line3 = $post_data[0]['line3'];
                $ratings = $post_data[0]['rating'];
                $comments  = $post_data[0]['comments'];
                $cmt_count = count($cmt_count);
                
                $tags_array =isset($post_data[0]['tags'])?$post_data[0]['tags']:array() ;

                $response['status']['code'] = 55;
                $response['status']['message'] = "Image match found.";


                 } else if(!$img_found && $data['msg_type'] ==1) {

                $info1['data'] = array();
                $comments=array();
                $rating=array();
                $time =time();
                

                 $ipaddress = '';
                if (isset($_SERVER['HTTP_CLIENT_IP']))
                    $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
                else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
                    $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
                else if(isset($_SERVER['HTTP_X_FORWARDED']))
                    $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
                else if(isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
                    $ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
                else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
                    $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
                else if(isset($_SERVER['HTTP_FORWARDED']))
                    $ipaddress = $_SERVER['HTTP_FORWARDED'];
                else if(isset($_SERVER['REMOTE_ADDR']))
                    $ipaddress = $_SERVER['REMOTE_ADDR'];
                else
                    $ipaddress = 'UNKNOWN';

                 //$user_prefix="user";
                 
                 // die(json_encode(++$userId ));
                  
                  $count=DB::collection('ip_collections')->where('ip',$ipaddress)->count();

                  if($count==0){

                    $userId =DB::collection('ip_collections')->max('userId');
                    $userId =$userId==null?1:$userId;   
                    DB::collection('ip_collections')->insertGetId(['ip' =>$ipaddress,'userId'=>++$userId ,'added_at' =>$time,'update_at' =>$time]);

                  }

                  $userName=DB::collection('ip_collections')->where('ip',$ipaddress)->get();
                  $userName='user'.$userName[0]['userId'];
                
                  $tagsarray=array();
                  $id=DB::collection('post_collection')->insertGetId(['userName' => $userName,'line1' => $line1, 'line2' => $line2,'line3' => $line3,'rating' => $rating,'comments' =>$comments,'tag' =>$tagsarray,'string' => $ocr_actual  ,'added_at' => $time ,'update_at' => $time ]);
                   
                  DB::collection('image_collection')->insertGetId(['imgid' => $id,'imgurl' => $imgurl]);

                  

                $ratings = array();
                
                 
            if(isset($data['tags'])){
            $tags = explode("#",$data['tags']);
            $t=0;
            foreach ($tags as $tag_data){
               if($t >0) {

                if (DB::collection('tags_collections')->where('tag', $tag_data)->count() == 1) {
                    $dbtags=DB::collection('tags_collections')->where('tag', $tag_data)->get();
                    $tagid=(string)$dbtags[0]['_id'];
                  

                }else { 
                      $tagid=DB::collection('tags_collections')->insertGetId(['tag' =>$tag_data]);
                     // die(json_encode( $id ));
                   

                }
                $tag_arr['tag']=$tag_data;
                $tag_arr['tagId']=(string)$tagid;
                array_push($tags_array, $tag_arr);

              
                // $tag_arr('');

               }else $t++; 
            }
           }
           
            DB::collection('post_collection')->where('_id', $id)->push('tags',$tags_array);
                
              //  $rating =  $info['rate'];
                $total = 1; //die(json_encode(  $rating ));
                $response['status']['code'] = 0;
                $response['status']['message'] = "String inserted to database.";
              
                 }


                 else if (DB::collection('post_collection')->where('line1', $line1)->where('line2', $line2)->where('line3', $line3)->count()  == 1) {

                $post_data = DB::collection('post_collection')->where('line1', $line1)->where('line2',$line2)->where('line3', $line3)->get();
              
                $image=DB::collection('image_collection')->where('imgid' ,  $post_data[0]['_id'])->get();
               
                $image_url= isset($image[0]['imgurl'])? $image[0]['imgurl']:"";
                $total = 0;
                $rate = 0; // die(json_encode(  $res));
                foreach ($post_data[0]['rating'] as $value) {
                    $total++;
                    $rate = $rate + $value['rate'];
                switch ($value['rate']) {
                  case 1 :
                   $downcount++;
                    break;
                 case 0 :
                     $upcount++;
                    break;
                
                  }
                }

                if($rate!=0) $rating = $rate / $total;  else  $rating =0;

                $id=(string)$post_data[0]['_id'];
                $line1 = $post_data[0]['line1'];
                $line2 = $post_data[0]['line2'];
                $line3 = $post_data[0]['line3'];
                $ratings = $post_data[0]['rating'];
                $comments  = $post_data[0]['comments'];
                $cmt_count = count($cmt_count);
                
                $tags_array =isset($post_data[0]['tags'])?$post_data[0]['tags']:array() ;

                $response['status']['code'] = 5;
                $response['status']['message'] = "String match found.";
                } else {
				  $image_url="";	
                  if ($data['msg_type'] == 3 ) {
                  if(!isset($data['image_info'])){
                  $response['status']['code'] = 1;
                  $response['status']['message']='image is not found';
                  die(json_encode($response));
                  }else {
					  $data['image_base64'] = base64_encode(file_get_contents($data['image_info']));
				$img1 = "truechecker/".time().'.jpg';
				$image = base64_decode($data['image_base64']);
				$t = Storage::disk('s3')->put($img1,$image, 'public');
				$image_url = Storage::disk('s3')->url($img1);
				  }
                  
                  }
                  
				
             
            
                $info1['data'] = array();
                $comments=array();
                $rating=array();
                $time =time();
                

                 $ipaddress = '';
                if (isset($_SERVER['HTTP_CLIENT_IP']))
                    $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
                else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
                    $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
                else if(isset($_SERVER['HTTP_X_FORWARDED']))
                    $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
                else if(isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
                    $ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
                else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
                    $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
                else if(isset($_SERVER['HTTP_FORWARDED']))
                    $ipaddress = $_SERVER['HTTP_FORWARDED'];
                else if(isset($_SERVER['REMOTE_ADDR']))
                    $ipaddress = $_SERVER['REMOTE_ADDR'];
                else
                    $ipaddress = 'UNKNOWN';

                 //$user_prefix="user";
                 
                 // die(json_encode(++$userId ));
                  
                  $count=DB::collection('ip_collections')->where('ip',$ipaddress)->count();

                  if($count==0){

                    $userId =DB::collection('ip_collections')->max('userId');
                    $userId =$userId==null?1:$userId;   
                    DB::collection('ip_collections')->insertGetId(['ip' =>$ipaddress,'userId'=>++$userId ,'added_at' =>$time,'update_at' =>$time]);

                  }

                  $userName=DB::collection('ip_collections')->where('ip',$ipaddress)->get();
                  $userName='user'.$userName[0]['userId'];
                 // die(json_encode( $userName));
                  $tagsarray=array();
                  $id=DB::collection('post_collection')->insertGetId(['userName' => $userName,'line1' => $line1, 'line2' => $line2,'line3' => $line3,'rating' => $rating,'comments' =>$comments,'tag' =>$tagsarray,'string' => $ocr_actual  ,'added_at' => $time ,'update_at' => $time ]);
                 
                  if($image_url!="") {
                    DB::collection('image_collection')->insertGetId(['imgid' => $id,'imgurl' => $image_url]);
			      }

                $ratings = array();
                
                 
            if(isset($data['tags'])){
            $tags = explode("#",$data['tags']);
            $t=0;
            foreach ($tags as $tag_data){
               if($t >0) {

                if (DB::collection('tags_collections')->where('tag', $tag_data)->count() == 1) {
                    $dbtags=DB::collection('tags_collections')->where('tag', $tag_data)->get();
                    $tagid=(string)$dbtags[0]['_id'];
                  

                }else { 
                      $tagid=DB::collection('tags_collections')->insertGetId(['tag' =>$tag_data]);
                     // die(json_encode( $id ));
                   

                }
                $tag_arr['tag']=$tag_data;
                $tag_arr['tagId']=(string)$tagid;
                array_push($tags_array, $tag_arr);

              
                // $tag_arr('');

               }else $t++; 
            }
           }
           
            DB::collection('post_collection')->where('_id', $id)->push('tags',$tags_array);
                
              //  $rating =  $info['rate'];
                $total = 1; //die(json_encode(  $rating ));
                $response['status']['code'] = 0;
                $response['status']['message'] = "String inserted to database.";
              
            }
            
           
          
        
           
            $response['data']['rating'] = round($rating, 2);
            $response['data']['postId'] = (string)$id;
            /* $response['data']['line1'] =  $line1;
            $response['data']['line2'] =  $line2;
            $response['data']['line3'] =  $line3;*/
            $response['data']['userId']   = $userId;
            $response['data']['ratings']  = $ratings;
            $response['data']['comments'] = $comments;
            $response['data']['downcount'] = $downcount;
            $response['data']['upcount']   = $upcount;
            $response['data']['cmt_count']   = $cmt_count;
            $response['data']['string']  =    $ocr_actual;
            $response['data']['image_post']  = $image_url;
            $response['data']['tags']   =    $tags_array;
            
            die(json_encode($response ));
      
    }
    public function post_rating(Request $request) {
        $inputs = $request->all();
       // die(jsadasd_sadsad)l
        $rules = array('postId' => 'required', 'rating' => 'required|numeric|min:0|max:1',);
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response['status']['code'] = 1;
            $res=$validator->getMessageBag()->toArray();
            foreach ($res as $key => $value) {  $response['status']['message']=$value[0];}
            return response()->json($response);
       }
            
            $downcount=$upcount=$cmt_count=0;  
            try {
            $data = $request->all();
                $ipaddress = '';
                if (isset($_SERVER['HTTP_CLIENT_IP']))
                    $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
                else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
                    $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
                else if(isset($_SERVER['HTTP_X_FORWARDED']))
                    $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
                else if(isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
                    $ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
                else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
                    $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
                else if(isset($_SERVER['HTTP_FORWARDED']))
                    $ipaddress = $_SERVER['HTTP_FORWARDED'];
                else if(isset($_SERVER['REMOTE_ADDR']))
                    $ipaddress = $_SERVER['REMOTE_ADDR'];
                else
                    $ipaddress = 'UNKNOWN';


            $info1['data'] = array();
            $time = time();
              
                  $count=DB::collection('ip_collections')->where('ip',$ipaddress)->count();

                  if($count==0){

                    $userId =DB::collection('ip_collections')->max('userId');
                    $userId =$userId==null?1:$userId;   

                   
                    DB::collection('ip_collections')->insertGetId(['ip' =>$ipaddress,'userId'=>++$userId ,'added_at' =>$time,'update_at' =>$time]);



                   }

                  $userName=DB::collection('ip_collections')->where('ip',$ipaddress)->get();
                  $userName='user'.$userName[0]['userId'];  
          
            $response['status']['code'] = 0;
            $response['status']['message'] = "Your rating is updated.";
            $data['userId']= $ipaddress;            
            if (isset($data['userId'])) {
                $userId = trim($data['userId']);
                if ($userId != "") {
                    $exist_rating = DB::collection('post_collection')->where('_id', $data['postId'])->where('rating.userId', $userId)->count();
                    //die(json_encode(   $exist_rating ));
                    if ($exist_rating >= 1) {
                        $response['status']['code'] = 2;
                        $response['status']['message'] = "You have already rated this...";
   
                    }else {
                    $info['userId']   = $ipaddress;
                    $info['userName'] = $userName;
                    $info['rate'] = (int)$data['rating'];
                    $info['userType'] = 0;
                    $info['ratedat'] = $time;
                    array_push($info1['data'], $info);
                    $get = DB::collection('post_collection')->where('_id', $data['postId'])->push('rating', $info1['data']);
                    }
                }
            }

            $post_data = DB::collection('post_collection')->where('_id', $data['postId'])->get();
         
            
            $total = 0;
            $rate = 0;
            if(isset($post_data[0]['rating'])) {
            foreach ($post_data[0]['rating'] as $value) {
                $total++;
                $rate = $rate + $value['rate'];
                 switch ($value['rate']) {
                  case 1 :
                   $downcount++;
                    break;
                 case 0 :
                     $upcount++;
                    break;
                }
            }}
            $rating = $total!=0? $rate / $total:0;
            $ratings = $post_data[0]['rating'];
            $comments = $post_data[0]['comments'];
          
            $response['data']['rating'] = round($rating, 2);
            $response['data']['postId'] = $data['postId'];
           /* $response['data']['line1'] =  $post_data[0]['line1'];
            $response['data']['line2'] =  $post_data[0]['line2'];
            $response['data']['line3'] =  $post_data[0]['line3'];
            $response['data']['userId'] = $userId;*/
            $response['data']['ratings'] =   $ratings;
            $response['data']['comments']  = $comments;
            $response['data']['downcount'] = $downcount;
            $response['data']['upcount']   = $upcount;
            $response['data']['string']    = $post_data [0]['string'];
            $response['data']['cmt_count'] = count($post_data[0]['comments']);

          
            
        }
        catch(\Exception $e) {
          $response['status']['code'] = 1;
           $response['status']['message'] = "Invalid PostId try again.";
       }
        die(json_encode($response));
    }




     public function post_comment(Request $request) { 
        $inputs = $request->all();
        $rules = array('postId' => 'required', 'comment' => 'required',);
               $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response['status']['code'] = 1;
            $res=$validator->getMessageBag()->toArray();
            foreach ($res as $key => $value) {  $response['status']['message']=$value[0];}
            return response()->json($response);
       }
            //die(json_encode($rules));
            $downcount=$upcount=$cmt_count=0;  
            try {
            $data = $request->all();
                $ipaddress = '';
                if (isset($_SERVER['HTTP_CLIENT_IP']))
                    $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
                else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
                    $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
                else if(isset($_SERVER['HTTP_X_FORWARDED']))
                    $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
                else if(isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
                    $ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
                else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
                    $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
                else if(isset($_SERVER['HTTP_FORWARDED']))
                    $ipaddress = $_SERVER['HTTP_FORWARDED'];
                else if(isset($_SERVER['REMOTE_ADDR']))
                    $ipaddress = $_SERVER['REMOTE_ADDR'];
                else
                    $ipaddress = 'UNKNOWN';


            $info1['data'] = array();
            $time = time();
              
                  $count=DB::collection('ip_collections')->where('ip',$ipaddress)->count();

                  if($count==0){

                    $userId =DB::collection('ip_collections')->max('userId');
                    $userId =$userId==null?1:$userId;   

                   
                    DB::collection('ip_collections')->insertGetId(['ip' =>$ipaddress,'userId'=>++$userId ,'added_at' =>$time,'update_at' =>$time]);



                   }

                  $userName=DB::collection('ip_collections')->where('ip',$ipaddress)->get();
                  $userName='user'.$userName[0]['userId'];  
          
            $response['status']['code'] = 0;
            $response['status']['message'] = "Your rating is updated.";
            $data['userId']= $ipaddress;            
            if (isset($data['userId'])) {
                $userId = trim($data['userId']);
                if ($userId != "") {
                    $exist_rating = DB::collection('post_collection')->where('_id', $data['postId'])->where('comments.userId', $userId)->count();
                    //die(json_encode(   $exist_rating ));
                    if ($exist_rating >= 1) {
                        $response['status']['code'] = 2;
                        $response['status']['message'] = "You have already rated this...";
   
                    }else {
                   $info['userId']   = $ipaddress;
            $info['userName'] = $userName;
            $info['comment']  = $data['comment'];
            $info['userType'] = 1;
            $info['commentat'] = $time;
                    array_push($info1['data'], $info);
                   $get = DB::collection('post_collection')->where('_id', $data['postId'])->push('comments', $info1['data']);
                    }
                }
            }

            $post_data = DB::collection('post_collection')->where('_id', $data['postId'])->get();
            $image=DB::collection('image_collection')->where('imgid' , $data['postId'])->get();
            $image= isset($image[0]['imgurl'])? $image[0]['imgurl']:"";
            //$value['image_post']=$image;
            
            $total = 0;
            $rate = 0;
            if(isset($post_data[0]['rating'])) {
            foreach ($post_data[0]['rating'] as $value) {
                $total++;
                $rate = $rate + $value['rate'];
                 switch ($value['rate']) {
                  case 1 :
                   $downcount++;
                    break;
                 case 0 :
                     $upcount++;
                    break;
                }
            }}
            $rating = $total!=0? $rate / $total:0;
            $ratings = $post_data[0]['rating'];
            $comments = $post_data[0]['comments'];
          
            $response['data']['rating'] = round($rating, 2);
            $response['data']['postId'] = $data['postId'];
           /* $response['data']['line1'] =  $post_data[0]['line1'];
            $response['data']['line2'] =  $post_data[0]['line2'];
            $response['data']['line3'] =  $post_data[0]['line3'];
            $response['data']['userId'] = $userId;*/
            $response['data']['ratings']   = $ratings;
            $response['data']['comments']  = $comments;
            $response['data']['downcount'] = $downcount;
            $response['data']['upcount']   = $upcount;
            $response['data']['image_post']   = $image;
            $response['data']['string']    = $post_data [0]['string'];
            $response['data']['cmt_count'] = count($post_data[0]['comments']);
            
        }
        catch(\Exception $e) {
          $response['status']['code'] = 1;
           $response['status']['message'] = "Invalid PostId try again.";
       }
        die(json_encode($response));
    }
   

   public  function get_post(Request $request) {
         $limit=10;
         $offset=1;
          $data = $request->all();
         if (isset($data['offset'])) {
            $offset = trim($data['offset']);
         }  
          $offset=($offset-1)*$limit;     
        $get = DB::collection('post_collection')->orderby('_id','desc')->take($limit)->skip($offset)->get();
        //die(json_encode( $offset));
          // die(json_encode(  $get ));
         $info['status']['code'] = 0;
         $info['status']['message'] = "Posted messages are ....";
         $info['data'] =array();
       
         foreach($get as &$value) {
            $value['timeago']   = TimeControl::time_ago($value['update_at']);
            $value['downcount'] = $value['upcount']=0;
         // die(json_encode((string)$value['_id']));
           
            $image=DB::collection('image_collection')->where('imgid' ,$value['_id'])->get();

         
            $image= isset($image[0]['imgurl'])? $image[0]['imgurl']:"";
            $value['image_post']=$image;
           
           foreach($value['rating'] as &$rate) {
               
            
            switch ($rate['rate']) {
                  case 1 :
                   $value['downcount']++;
                    break;
                 case 0 :
                     $value['upcount']++;
                    break;
                
               }

            }
            $value['_id']=    (string)$value['_id'];
            $value['cmt_count']=count($value['comments']);
            array_push($info['data'],$value);
           }

           return $info;
   }
  
}
