<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use DB;
use App\MasterPhone;
 
class WhitePhoneControl extends Controller
{ 
    
      public function white_phone(Request $request) {
       
        $rules = array(
             'phone_num' => 'required',
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
          $res = $validator->getMessageBag()->toArray();
            foreach ($res as $key => $value) {
                $response['status']['message'] = $value[0];
            }
            $response['status']['message'] = $value[0];
            return response()->json($response);
        } 
        $data = $request->all();
        
        
         $response['status']['code']=0;
         $response['status']['message']="Users details found.";
          
        
          try {          
          $result=file_get_contents('https://proapi.whitepages.com/3.0/phone?phone='.$data['phone_num'].'&api_key=bf70a99f56f24ed5995c42996a0ced3f');
          $result =json_decode($result, true);
        

           $master_mobile =  MasterPhone::firstOrNew(['mobile' => $result['phone_number']]); 
           $master_mobile->mobile  =isset($result['phone_number'])?$result['phone_number']:"";
           $master_mobile->tcname  =isset($result['belongs_to'][0]['name'])?$result['belongs_to'][0]['name']:"";
           $master_mobile->area    =isset($result['current_addresses'][0]['city'])?$result['current_addresses'][0]['city']:"";
           $master_mobile->city    =isset($result['current_addresses'][0]['city'])?$result['current_addresses'][0]['city']:"";
           $master_mobile->countryCode =isset($result['country_calling_code'])?$result['country_calling_code']:"";
           $master_mobile->email    = "";
           $master_mobile->operator =isset($result['carrier'])?$result['carrier']:"";
           $master_mobile->facebook_id =  "";
           $master_mobile->timage   = "";
           $master_mobile->response =$result;
           $master_mobile->save();
         
         
       
           
       
             
       if(isset($GLOBALS['apikeyID'])){

                $ipaddress = '';
                if (isset($_SERVER['HTTP_CLIENT_IP']))
                    $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
                else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
                    $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
                else if(isset($_SERVER['HTTP_X_FORWARDED']))
                    $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
                else if(isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
                    $ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
                else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
                    $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
                else if(isset($_SERVER['HTTP_FORWARDED']))
                    $ipaddress = $_SERVER['HTTP_FORWARDED'];
                else if(isset($_SERVER['REMOTE_ADDR']))
                    $ipaddress = $_SERVER['REMOTE_ADDR'];
                else
                    $ipaddress = 'UNKNOWN';
          

             $apikeyID=$GLOBALS['apikeyID'];
             $insert_db=array(
              'apikeyID'=>$apikeyID,
              'requesttime'=>time(),
              'requestIP'=>$ipaddress,
              'response' =>$result['data'],
             );
       
        
            DB::collection('apikeys')->where('_id',$apikeyID)->decrement('remcount');
            DB::collection('apis_analytics')->insertGetId($insert_db);
           } $response['data']= $result;
          } catch (\Exception $e) {
                 $response['status']['code']=2;
                 $response['status']['message']="No data Found.";
                  $response['status']['data']=array();

         }
          
        die(json_encode( $response));
      }
    
    
   
   
}
