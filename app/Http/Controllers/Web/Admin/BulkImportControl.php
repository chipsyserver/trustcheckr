<?php
namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Excel;
use DB;
use App\Repository\lib\EmailSearch_lib;
use App\Repository\lib\PhoneSearch_lib;
class BulkImportControl extends Controller
{  
   public function load_email_upload(Request $request){
       $get= DB::collection('bulk_main_mail')->get();
        return view('Admin.pages.display_emailupload')->with("response",$get);
   }

   public function load_phone_upload(Request $request){
         $get= DB::collection('bulk_main_phone')->get();
         return view('Admin.pages.display_phoneupload')->with("response",$get);
   }
    public function phone_upload(Request $request){

    $rules = array('name' => 'required','import_file' => 'required');
    $validator = Validator::make($request->all(), $rules);
    if ($validator->fails()) {
            $res=$validator->getMessageBag()->toArray();
            foreach ($res as $key => $value) {  return back()->with('error',$value[0]);  } 
    }
 
      set_time_limit(300);
      if($request->hasFile('import_file')){ 
      $post=$request->all();         
      $path = $request->file('import_file')->getRealPath();
      $data = Excel::load($path, function($reader) {})->get();
      $res['data']=array();
      $x=0;

     if(!empty($data) && $data->count()>0){
       foreach($data as $value) {  
       
       if($value['phone']!=""){   
       if($x==0){
        $upid=DB::collection('bulk_main_phone')->insertGetId(array(
             'name'=> $post['name'],
             'state'=>0,
             'added_at'=>time(),
            ));
       }
                                     $insert['bulkId']=(string)$upid;
                                     $insert['phone_num']=trim((string)$value['phone']);
                                     $insert['res_state']=0;
                                     $insert['response']=array();
                                     DB::collection('bulk_child_phone')->insertGetId( $insert);
                                     $x++;
                                   }
                                    
                    }
        }else {
       return back()->with('error','Please upload atleast on email'); 
    }
       
         return back()->with('success','Insert Record successfully.');
      }
    }
     public function phone_upload_byid(Request $request,$id){
        ini_set('max_execution_time', 2400);
        $getfile= DB::collection('bulk_main_phone')->where('_id',$id)->first();
        $data= DB::collection('bulk_child_phone')->where('bulkId',$id)->get();
       // die(json_encode( $data ));
         Excel::create($getfile['name'], function($excel) use($data) {
          $excel->sheet('Sheetname', function($sheet) use($data) {
          $sheet->row(1, array('slno',
          'phone', 'name', 'confidence','default_carrier','location','gender','city','email','facebook_id','facebook_name'));
           
            $i=2; 
            foreach($data as $info){ 
             $getdata=$info['response'];
             if($info['res_state']==0){
                 
             $res=PhoneSearch_lib::phonesearchlib_func($info);
      
             if(count($res['status']['data'])>0){
                 
                DB::collection('bulk_child_phone')
                ->where('_id',$info['_id'])
                ->update(array(
                         'res_state'=>1,
                         'response'=>$res['status']['data'],
                  )); 
                  
                 
                  $getdata=$res['status']['data'];
                 }
              } 

             
              
              $sheet->row($i++,array(
                    ($i-2),
                    $info['phone_num'],
                    isset($getdata['value']['name'])?$getdata['value']['name']:"",
                    isset($getdata['value']['confidence'])?$getdata['value']['confidence']:"",
                    isset($getdata['value']['number_details'][0]['default_carrier'])?$getdata['value']['number_details'][0]['default_carrier']:"",
                    isset($getdata['value']['number_details'][0]['location'])?$getdata['value']['number_details'][0]['location']:"",
                    isset($getdata['value']['gender_details'][0]['gender'])?$getdata['value']['gender_details'][0]['gender']:"",
                    isset($getdata['value']['location_details'][0]['city'])?$getdata['value']['location_details'][0]['city']:"",
                    isset($getdata['value']['social_details'][0]['email'])?$getdata['value']['social_details'][0]['email']:"",
                    isset($getdata['value']['social_details'][1]['facebook_id'])?$getdata['value']['social_details'][1]['facebook_id']:"",
                    isset($getdata['value']['social_details'][1]['facebook_name'])?$getdata['value']['social_details'][1]['facebook_name']:"",
                    
                    ));
                  
             
                 }
                
                 DB::collection('bulk_main_phone')
                  ->where('_id',$data[0]['bulkId'])
                  ->update(array(
                         'state'=>1,
                   ));

          });
      })->export('xls');
   
   }
    
   public function email_upload_byid(Request $request,$id){
      ini_set('max_execution_time', 2400);
        $getfile= DB::collection('bulk_main_mail')->where('_id',$id)->first();
        $data= DB::collection('bulk_child_mail')->where('bulkId',$id)->get();
       // die(json_encode($get));
        Excel::create($getfile['name'], function($excel) use($data) {
          $excel->sheet('Sheetname', function($sheet) use($data) {
          $sheet->row(1, array(
          'email', 'account', 'domain','status','sub_status','firstname','lastname' ,'displayName','gender','location','kind','id','aboutme','url','image','organizations','placesLived','circledByCount'));
           
            $i=2;
            foreach($data as $info){
             $getdata=$info['response'];
             if($info['res_state']==0){
            
             $res=EmailSearch_lib::emailsearchlib_func($info);
             if(count($res['status']['data'])>0){
                 
                DB::collection('bulk_child_mail')
                ->where('_id',$info['_id'])
                ->update(array(
                         'res_state'=>1,
                         'response'=>$res['status']['data'],
                  )); 
                  
                 
                  $getdata=$res['status']['data'];

               }



              
             } 
             
             $sheet->row($i++,array(
                     $info['email'],
                    isset($getdata['email_check']['account'])?$getdata['email_check']['account']:"",
                   isset($getdata['email_check']['domain'])?$getdata['email_check']['domain']:"",
                    isset($getdata['email_check']['status'])?$getdata['email_check']['status']:"",
                   isset($getdata['email_check']['sub_status'])?$getdata['email_check']['sub_status']:"",
                    isset($getdata['email_check']['firstname'])?$getdata['email_check']['firstname']:"",
                   isset($getdata['email_check']['lastname'])?$getdata['email_check']['lastname']:"",
                     isset($getdata['googleapis']['displayName'])? $getdata['googleapis']['displayName']:"" ,
                    isset($getdata['email_check']['gender'])?$getdata['email_check']['gender']:"",
                    isset($getdata['email_check']['location'])?$getdata['email_check']['location']:"",
                    isset($getdata['googleapis']['kind'])?$getdata['googleapis']['kind']:"",
                    isset($getdata['googleapis']['id'])?$getdata['googleapis']['id']:"",
                  isset($getdata['googleapis']['aboutMe'])?$getdata['googleapis']['aboutMe']:"",
                  isset($getdata['googleapis']['organizations'])? $getdata['googleapis']['url']:"" ,
                  isset($getdata['googleapis']['organizations'])?json_encode($getdata['googleapis']['image']):"" ,
                  isset($getdata['googleapis']['organizations'])?json_encode($getdata['googleapis']['organizations']):"" ,
                  isset($getdata['googleapis']['placesLived'])?json_encode($getdata['googleapis']['placesLived']):"",
                  isset($getdata['googleapis']['circledByCount'])?$getdata['googleapis']['circledByCount']:"",
               
                  ));
                  //die(json_encode($getdata));
                   //break;
                 }
              DB::collection('bulk_main_mail')
                ->where('_id',$data[0]['bulkId'])
                ->update(array(
                         'state'=>1,
                        
                  ));

          });
      })->export('xls');
   
   }

   public function email_upload(Request $request){

    $rules = array('name' => 'required','import_file' => 'required');
    $validator = Validator::make($request->all(), $rules);
    if ($validator->fails()) {
            $res=$validator->getMessageBag()->toArray();
            foreach ($res as $key => $value) {  return back()->with('error',$value[0]);  } 
    }
 
      set_time_limit(300);
      if($request->hasFile('import_file')){ 
      $post=$request->all();         
      $path = $request->file('import_file')->getRealPath();
      $data = Excel::load($path, function($reader) {})->get();
      $res['data']=array();
      $x=0;
     if(!empty($data) && $data->count()>0){
       foreach($data as $value) {  
       
       if($value['email']!=""){   
       if($x==0){
        $upid=DB::collection('bulk_main_mail')->insertGetId(array(
             'name'=> $post['name'],
             'state'=>0,
             'added_at'=>time(),
            ));
       }
                                     $insert['bulkId']=(string)$upid;
                                     $insert['email']=trim((string)$value['email']);
                                     $insert['res_state']=0;
                                     $insert['response']=array();
                                     DB::collection('bulk_child_mail')->insertGetId( $insert);
                                     $x++;
                                   }
                                    
                    }
        }else {
       return back()->with('error','Please upload atleast on email'); 
    }
       
         return back()->with('success','Insert Record successfully.');
      }
    }



  
 
}
