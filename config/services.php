<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    
    'facebook' => [
        'client_id' => '1544406792283232',
        'client_secret' => '1451bc1351b7949b59c955708ea82bd0',
        'redirect' => 'http://www.profilecheckr.com/facebook-controls',
    ],

 
    'linkedin' => [
        'client_id'     => '81o0e6xpucrd2l',
        'client_secret' => 'vqNNVVYR2860VL7m',
        'redirect'      => 'http://localhost:8000/auth/linkedin/callback',
        'scope'         => 'r_basicprofile r_emailaddress r_contactinfo r_fullprofile',
    ],


     'google' => [
        'client_id'     => '922808415077-befa2i947oc1ise6pskb2e5c590ip772.apps.googleusercontent.com',
        'client_secret' => 'cehDEK3mZTC0uNwNFaY3jbek',
        'redirect'      => 'http://localhost:8000/auth/linkedin/callback',
        'scope'         => 'r_basicprofile r_emailaddress r_contactinfo r_fullprofile',
    ],


    
   
];
